namespace UnitTests
{
    using HelloWorld;

    public class ProgramTests
    {
        [Fact]
        public void Main_EmptyArray_ExcutesIt()
        {
            // Arrange
            var args = Array.Empty<string>();

            // Act
            Program.Main(args);
        }
    }
}